package Tests;

import APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme.AbstractCurrencyTransaction;
import org.junit.Assert;
import org.junit.Test;

import static Util.getAtomicDataForTask.getAtomicDataForTask;

public class TestCurrencyCodeMatch {


    @Test
    public void codeAndNameMatchTest() throws Exception {
        for (AbstractCurrencyTransaction fromAndToCurrency : getAtomicDataForTask()) {
            switch (fromAndToCurrency.getCode()) {
                case 643:
                    Assert.assertEquals("RUB", fromAndToCurrency.getName());
                    break;
                case 840:
                    Assert.assertEquals("USD", fromAndToCurrency.getName());
                    break;
                case 978:
                    Assert.assertEquals("EUR", fromAndToCurrency.getName());
                    break;
                case 826:
                    Assert.assertEquals("GBP", fromAndToCurrency.getName());
                    break;
                default:
                    System.out.println("Unexpected code");
            }
        }
    }
}