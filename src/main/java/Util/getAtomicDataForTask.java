package Util;


import APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme.AbstractCurrencyTransaction;
import APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme.Rate;
import APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme.ResultCode;
import com.google.gson.Gson;

import java.util.ArrayList;

import static Util.Parser.readJsonFrom;

/**
 * Only one method, to get minimal needed object of currency code and name pairs for task.
 */
public final class getAtomicDataForTask {
    private getAtomicDataForTask() {
    }

    public static ArrayList<AbstractCurrencyTransaction> getAtomicDataForTask() throws Exception {
        ResultCode rootOfJsonScheme = new Gson().fromJson(readJsonFrom
                ("https://www.tinkoff.ru/api/v1/currency_rates/"), ResultCode.class);

        ArrayList<AbstractCurrencyTransaction> transactions = new ArrayList<AbstractCurrencyTransaction>();
        for (Rate rate : rootOfJsonScheme.getPayload().getRates()) {
            transactions.add(rate.getFromCurrency());
            transactions.add(rate.getToCurrency());
        }
        return transactions;
    }
}