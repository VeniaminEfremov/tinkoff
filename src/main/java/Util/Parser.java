package Util;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Contains static methods for json parsing.
 */
public class Parser {
    private Parser() {
    }

    public static String readJsonFrom(String url) throws Exception {
        InputStream is = new URL(url).openStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        return readAll(rd);
    }

    private static String readAll(Reader stream) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = stream.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}