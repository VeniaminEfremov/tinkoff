package APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastUpdate {
    @SerializedName("milliseconds")
    @Expose
    private long milliseconds;

    public long getMilliseconds() {
        return milliseconds;
    }
}