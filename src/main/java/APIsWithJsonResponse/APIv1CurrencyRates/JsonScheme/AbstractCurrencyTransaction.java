package APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme;

import com.google.gson.annotations.SerializedName;

/**
 * Breaking POJO rules for clarity and usability of code.
 */
public abstract class AbstractCurrencyTransaction {

    @SerializedName("code")
    int code;

    @SerializedName("name")
    String name;

    public int getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }
}