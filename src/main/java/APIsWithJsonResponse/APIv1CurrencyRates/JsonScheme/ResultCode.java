package APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Base(root) class in json scheme "tree".
 */
public class ResultCode {

    @SerializedName("resultCode")
    @Expose
    private String resultCode;

    @SerializedName("payload")
    @Expose
    private Payload payload;

    public String getResultCode() {
        return resultCode;
    }

    public Payload getPayload() {
        return payload;
    }
}