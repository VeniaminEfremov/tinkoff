package APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Payload {

    @SerializedName("lastUpdate")
    @Expose
    private LastUpdate lastUpdate;

    @SerializedName("rates")
    @Expose
    private List<Rate> rates;

    public LastUpdate getLastUpdate() {
        return lastUpdate;
    }

    public List<Rate> getRates() {
        return rates;
    }
}