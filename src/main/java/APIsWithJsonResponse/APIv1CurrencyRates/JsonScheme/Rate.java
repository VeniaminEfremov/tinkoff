package APIsWithJsonResponse.APIv1CurrencyRates.JsonScheme;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rate {
    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("fromCurrency")
    @Expose
    private FromCurrency fromCurrency;

    @SerializedName("toCurrency")
    @Expose
    private ToCurrency toCurrency;

    @SerializedName("buy")
    @Expose
    private double buy;

    public String getCategory() {
        return category;
    }

    public FromCurrency getFromCurrency() {
        return fromCurrency;
    }

    public ToCurrency getToCurrency() {
        return toCurrency;
    }

    public double getBuy() {
        return buy;
    }
}